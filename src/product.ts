import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productsRespository = AppDataSource.getRepository(Product)
 
 
    console.log("Loading products from the database...")
    const products = await productsRespository.find()
    console.log("Loaded products: ", products)

    const updatedProduct = await productsRespository.findOneBy({id: 1})
    console.log(updatedProduct)
    updatedProduct.price=80
    await productsRespository.save(updatedProduct)
}).catch(error => console.log(error))
